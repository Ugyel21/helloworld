//jshint esversion:6
const express = require("express");
const {
  matchedData,
  validationResult
} = require("express-validator");
const {bookingValidation} = require("../validator/bookingValidation");
const Booking = require("../models/bookingModel");
const crypto = require("crypto");
const router = express.Router();
const {transporter} = require("../config/email");
const app = express();

router.get("/bookingForm", function(req, res) {
  res.render("bookingForm");
});

router.post("/bookingForm", bookingValidation, function(req, res){
  try{
    const errors = validationResult(req);
    if(!errors.isEmpty()){
      var errMsg = errors.mapped();
      var inputData = matchedData(req);
      res.render("bookingForm", {errors: errMsg, inputData: inputData});
    }else{
      Booking.findOne({email: req.body.email}, function(err, foundUser){
        if(err){
          res.render("bookingForm", {errorMessage: err});
        }
        else if(foundUser){
          res.render("bookingForm", {errorMessage: "This email is already  register for booking. Please try with different email"});
        }else{
          const booking = Booking({
            name: req.body.name,
            email: req.body.email,
            phone: req.body.phone,
            date: req.body.date,
            time: req.body.time,
            day: req.body.day,
            emailToken: crypto.randomBytes(64).toString("hex"),
            isConfirm: false,
            payment: false
          });
          booking.save(function(err){
            if(err){
              var errorMessage = err;
              res.render("bookingForm", {errorMessage: errorMessage});
            }
            else{
              var mailOptions = {
                from: req.body.email,
                to: process.env.auth_user,
                subject: "Gyelpozhing Turf Booking - Booking Requested",
                html: "<h3> From: "+req.body.email+"</h3>Hello Admin, i am " + req.body.name + ", i want to booked your ground la. Can you please go through my request form la.<br>Thank you la."
              };

              //sending mail
              var sendingMail = transporter.sendMail(mailOptions, function(error, info) {
                if (error) {
                  res.render("bookingForm", {successMessage: error});
                } else {
                  res.render("bookingForm", {successMessage: "You have successfully submitted your form, Please wait for our confirmation. We will sent your confirmation link through your submitted email. Thank you for using our service."});
                }
              });
            }
          });
        }
      });
    }
  }catch(err){
    res.render("bookingForm", {errorMessage: err});
  }
});


module.exports = router;
