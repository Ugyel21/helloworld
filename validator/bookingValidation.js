//jshint esversion:6

const {
  check,
  sanitizedBody,
} = require("express-validator");

const bookingValidation = [
  //Full Name validation
  check("name").trim().notEmpty().withMessage("Full Name is required!"),
  //Email || email validation
  check("email").notEmpty().withMessage("Email Address is required!").normalizeEmail().isEmail().withMessage("Email address must be valid"),
  //Phone number validation
  check("phone").notEmpty().withMessage("Phone Number is required!"),
  check("date").trim().notEmpty().withMessage("Date is required!"),
  check("time").trim().notEmpty().withMessage("Time is required!"),
  check("day").trim().notEmpty().withMessage("Day is required!"),
];

// const loginValidation = [
//   //Email || email validation
//   check("email").trim().notEmpty().withMessage("Email Address is required!").normalizeEmail().isEmail().withMessage("Email address must be valid"),
//   //Password validation
//   check("password").trim().notEmpty().withMessage("Password is required!").isLength({
//     min: 5
//   }).withMessage("Password must be minimum 5 characters long"),
// ];

module.exports = {bookingValidation};
