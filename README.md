# CSF202 Agile Software Engineering Practice
*Gyalpozhing College of Information Technology*

## About the project 

This project is intended to be used as a teaching case study in CSF202, in seventh semester course. It is aimed at the studnets of Gyalpozhing College of Information Technology to learn how to use agile framework and how to create a website in a docker using Express js, node js and mongo db.

## Group 3
## Topic of project - Gyalpozhing Turf Booking
The lone turf in Gyalpozhing town has up to now been reserved on a first come, first basis. The turf manager has accomplished this by setting up a group chat on Telegram. The turf is offered both in the summer and the winter. The following team must wait for their turn if the field is booked.
Therefore, this Gyalpozhing Turf Booking website is proposed for booking the turf in an easy and efficient way.

## Project Team Members

Project Guide - Mrs. Jamyang Choden
<br>
<img src="Images/team_jamyang_choden.jpg" width="200" height="200" />
<br>
Product Owner - Mrs. Pema Zangmo (12190069)
<br>
<img src="Images/team_pz.jpeg" width="200" height="250" />
<br>
Scrum Master - Mr. Karma Choda (12190055)
<br>
<img src="Images/team_kc.jpeg" width="200" height="250" />
<br>
Developer - Mrs. Cheki Lhamo (12190043)
<br>
<img src="Images/team_cl.jpeg" width="200" height="200" />
<br>
Developer - Mr. Dorji Gyeltshen (12190049)
<br>
<img src="Images/team_dg.jpeg" width="200" height="200" />
<br>
Developer - Mr. Kinzang Dorji (12190061)
<br>
<img src="Images/team_kd.jpeg" width="200" height="200" />
<br>


<!-- ## Click the links given below:
To view app folder :
(https://gitlab.com/miniproject_loanPrediction/driver-drowsiness-detection/-/tree/main/Mobile%20Application)
<br>
<br>
To view machine learning model folder:
(https://gitlab.com/miniproject_loanPrediction/driver-drowsiness-detection/-/tree/main/Model%20Development)
<br>
<br>
Click Here to watch promotional Video:
(https://youtu.be/CDIeRgd_5qc)
<br>
<br> -->

